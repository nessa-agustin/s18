// console.log('test');

/*  Function able to receive data without the use of global variables or prompt()
 */

// name - is a parameter
/* parameter - is a variable/container that exists only in our function and is used to store information that is provided toa function when it is called/invoked */

function printName(name){
    console.log('My name is ' + name);
}

/*  Data passed into a function invocation can be received by the function

 */
printName("Jungkook");
printName('Nessa');

function printMyAge(age){
    console.log("I am " + age);
}

printMyAge(25);
// printMyAge();


function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log('The remainder of ' + num + " divided by 8 is : " + remainder);
    console.log(" Is " + num + " divisible by 8?");
    let isDivisibleBy8 = remainder === 0;
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

/* 
    Mini-Activity
    1. Create a function which is capable to receive data as an argument:

*/

function getMyFaveSuperhero(superhero){
    console.log('Your fave superhero is ' + superhero);
}

getMyFaveSuperhero('Dr Strange');

function checkEvenNumber(num){
    let remainder = num % 2;
    let isEven = remainder === 0;

    console.log('Is ' + num + ' an even number?');
    console.log(isEven);

}

checkEvenNumber(40);
checkEvenNumber(53);


function printFullName(firstName, middleInitial, lastName){

    console.log(firstName + ' ' + middleInitial + ' ' + lastName);
}

printFullName('Juan','Crisostomo','Ibarra');

/* Parameters will contain 

In other language, providing more/less arguments than the expected parameters sometimes causes an error or changes the behavior of the function
*/

printFullName('Stephen','Wardell');

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName);


// Return Statement
/*  Currently, our function are able to display data in our console. however our function cannot yet retunr values which can be savd intoa variable using the return statement/keyword
 */


let fullName = printFullName('Gabrielle', 'Vernadette','Fernandez');
console.log(fullName);


function returnFullName(firstName, middleName, lastName){

    return firstName + ' ' + middleName + ' ' + lastName;
}

fullName = returnFullName('Gabrielle', 'Vernadette','Fernandez');
console.log(fullName);

/* 
Mini-activity


*/


function getSum(num1, num2){
    let sum = num1 + num2;

    return sum;

}

let returnSum = getSum(40,20);
console.log(returnSum);




function getProduct(num1, num2){
    return num1 * num2;
};

let product = getProduct(5,3);
console.log(product);


/*
  Create a function which wikk be able to get the area of a circle from a provided radius
     - a number should be provided as an argument
     - look up for the formula for calculating the area of a circle 
     - look up for the use of exponent operator
     - you can save the value of the calculations in a variable
     - return the result of the area 

   Create a global variable called outside of the function called circleArea
    - this variable should be able to receive and store the result of the calculation of area of a circle
   Log the value of the circleArea in the console.

 */


   function getCircleArea(radius){
        let pi = 3.14;
        let result = pi * (Math.pow(radius,2));

        return result;


   }

   let circleArea = getCircleArea(2);
   console.log(circleArea);


   /* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */




function getScoreResult(score, totalScore){
    let passingRate = 75;
    let result = (score / totalScore) * 100;

    let isPassed = result > passingRate;

    return isPassed;

}

let isPassingScore = getScoreResult(80, 85);
console.log(isPassingScore);















